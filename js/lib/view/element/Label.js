/*
 * label元素类
 * @author: yrh
 * @create: 2016/7/20
 * @update: 2016/7/20  `
 */
define([
    'lib/view/View',
], function(BaseView) {
    var View = BaseView.extend({
    	tagName: 'label',
        initialize: function(option) {
            this.parent(option);
        }
    });
    return View;
});
