/*
 * 进度条通用组件类
 * @author: yrh
 * @create: 2016/7/21
 * @update: 2016/7/21
* options: {
    size: '', //sm, xs
    bar: {
        className: '',
        width: 0,
        style: {}
    }
}
 */
define([
    'lib/view/View',
], function(BaseView) {
    var View = BaseView.extend({
        className: 'progress',
        initialize: function(option) {
            var that = this,
                defaults = {
                    options: {
                        size: '', //sm, xs
                        bar: {
                            className: '',
                            width: 0,
                            style: {}
                        }
                    }
                };
            if (option) $.extend(true, defaults, option);
            this.context = option.context;
            this.parent(defaults);
            this.renderAll();
        },
        // 设置进度值
        setProgress: function(data) {
            var width = data.width || 0,
                index = data.index || 0,
                progressbar = index ? this.$('#' + this.id + '_' + index + '_progressbar') : this.$('.progress-bar');
            progressbar.width(width);
            if (this.options.size) {
                var progressVal = $('<span class="sr-only"/>');
                progressVal.text(width || 0);
                progressbar.html(progressVal);
            } else {
                progressbar.text(width || 0);
            }
        },
        renderAll: function() {
            var that = this,
                size = this.options.size,
                bar = this.options.bar;
            if (size) this.$el.addClass('progress-' + size);
            if (bar) {
                var barArr = [];
                if(!_.isArray(bar)) barArr.push(bar);
                _.each(barArr, function(thebar, index) {
                    var theId = that.id + '_' + index + '_progressbar',
                        progressbar = $('<div class="progress-bar" id="' + theId + '" />');
                    if (thebar.className) progressbar.addClass(thebar.className);
                    if (thebar.style) progressbar.css(thebar.style);
                    that.$el.append(progressbar);
                    that.setProgress({
                        width: thebar.width,
                        index: index
                    });
                });
            }
        }
    });
    return View;
});
