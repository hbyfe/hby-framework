/*
 * 输入框元素类
 * @author: yrh
 * @create: 2016/7/16
 * @update: 2016/7/16
 */
define([
    'lib/view/View',
], function(BaseView) {
    var View = BaseView.extend({
        tagName: 'input',
        initialize: function(option) {
            this.parent(option);
            this.$el.attr('type', this.options.type || 'text');
            if (this.options.value) this.$el.val(this.options.value || '');
            if (this.options.placeholder) this.$el.attr('placeholder', this.options.placeholder);
            if (this.options.disabled) this.$el.attr('disabled', this.options.disabled);
            if (this.options.readonly) this.$el.attr('readonly', this.options.readonly);
            if (this.options.required) this.$el.attr('required', this.options.required);
            if (this.options.width) this.$el.width(this.options.width);
            this.$el.addClass(this.options.className || 'form-control');
        }
    });
    return View;
});
