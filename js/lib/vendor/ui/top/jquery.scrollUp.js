define([
    'jquery',
], function($) {
	$.fn.scrollUp = function(t) {
		var n = {
			scrollName: "scrollUp",
			topDistance: 300,
			topSpeed: 300,
			animation: "fade",
			animationInSpeed: 200,
			animationOutSpeed: 200,
			scrollText: "Scroll to top",
			scrollImg: false,
			activeOverlay: false
		};
		var r = $.extend({}, n, t),
			i = "#" + r.scrollName;
		$("<a/>", {
			id: r.scrollName,
			href: "#top",
			title: r.scrollText
		}).appendTo("body");
		if (!r.scrollImg) {
			$(i).text(r.scrollText)
		}
		$(i).css({
			display: "none",
			position: "fixed",
			"z-index": "2147483647"
		});

		$(i).append("<span class='glyphicon glyphicon-circle-arrow-up'></span>");
		if (r.activeOverlay) {
			$("body").append("<div id='" + r.scrollName + "-active'></div>");
			$(i + "-active").css({
				position: "absolute",
				top: r.topDistance + "px",
				width: "100%",
				"border-top": "1px dotted " + r.activeOverlay,
				"z-index": "2147483647"
			});
		}
		$(window).scroll(function() {
			switch (r.animation) {
				case "fade":
					$($(window).scrollTop() > r.topDistance ? $(i).fadeIn(r.animationInSpeed) : $(i).fadeOut(r.animationOutSpeed));
					break;
				case "slide":
					$($(window).scrollTop() > r.topDistance ? $(i).slideDown(r.animationInSpeed) : $(i).slideUp(r.animationOutSpeed));
					break;
				default:
					$($(window).scrollTop() > r.topDistance ? $(i).show(0) : $(i).hide(0))
			}
		});
		$(i).click(function(t) {
			$("html, body").animate({
				scrollTop: 0
			}, r.topSpeed);
			t.preventDefault()
		});
	};
});