/*
 * 表单组件
 * @author: yrh
 * @create: 2016/7/19
 * @update: 2016/7/19
 * ===============================
 * options: {
    form: {}, //表单
    submitEl: '', //提交表单按钮
    layout: {
        type: 'default', //horizontal,inline
        rows: [{
            isMust: false, //是否必填
            label: {
                text: '',
                html: ''
            },
            input: {},
            help: {}
        },{
            legend: '',
            rows: [{
                isMust: false,
                label: {},
                input: {}
            }]
        }]
    },
    components: [{

    }]
}
 * ===============================
 */
define([
    'lib/view/View',
    'lib/view/element/Form'
], function(BaseView, FormView) {
    var View = BaseView.extend({
        initialize: function(option) {
            var that = this,
                defaults = {
                    model: null,
                    options: {
                        form: {}, //表单
                        submitEl: '[type="submit"]', //提交表单按钮
                        layout: {
                            type: '', //horizontal,inline
                        },
                        components: [],
                        onSuccess: function() {},
                        onError: function() {}
                    }
                };
            if (option) $.extend(true, defaults, option);
            this.context = option.context;
            this.model = option.model;
            this.parent(defaults);
            this.isSended = false; //是否已发送
            if (!this.model) return false;
            this.renderAll();
        },
        // 添加行
        addOne: function(row, index) {
            this.$('#' + this.id + '_form').append(this._makeRow(row, index));
        },
        // 生成行
        _makeRow: function(row, index) {
            var rowEl = $('<div class="form-group"></div>'),
                labelEl = $('<label/>'),
                inputEl = $('<div class="form-input"><div id="' + this.id + '_' + index + '"></div></div>'),
                helpEl = $('<span class="help-block"/>'),
                labelText = '',
                type = this.options.layout.type;
            if (type == 'horizontal') {
                if (_.isObject(row.label)) {
                    labelText = row.label.html ? row.label.html : (row.label.text || '');
                    labelEl.addClass((row.label.className || 'col-sm-2') + ' control-label')
                        .html(labelText);
                    if (row.isMust) labelEl.append('<span class="symbol required"></span>');
                    rowEl.append(labelEl);
                }
                if (_.isObject(row.input)) {
                    inputEl.addClass(row.input.className || ((!row.label ? 'col-sm-offset-2 ' : '') + 'col-sm-10'));
                    rowEl.append(inputEl);
                }
            } else {
                if (_.isObject(row.label)) {
                    labelText = row.label.html ? row.label.html : (row.label.text || '');
                    labelEl.html(labelText);
                    if (row.isMust) labelEl.append('<span class="symbol required"></span>');
                    rowEl.append(labelEl);
                }
                if (_.isObject(row.input)) rowEl.append(inputEl);
            }
            if (_.isObject(row.help)) {
                var helpText = row.help.html ? row.help.html : (row.help.text || '');
                helpEl.addClass(row.help.className || '').html(helpText);
                inputEl.append(helpEl);
            }
            return rowEl;
        },
        // 渲染行
        _renderRows: function(rows) {
            var that = this,
                container = this.$('#' + this.id + '_form');
            if (rows.length) {
                _.each(rows, function(row, index) {
                    if (row.legend) {
                        var fieldsetEl = $('<fieldset/>'),
                            legendEl = $('<legend>' + row.legend + '</legend>');
                        fieldsetEl.html(legendEl);
                        if (row.rows.length) {
                            _.each(row.rows, function(item, i) {
                                fieldsetEl.append(that._makeRow(item, index + '_' + i));
                            });
                        }
                        container.append(fieldsetEl);
                    } else {
                        container.append(that._makeRow(row, index));
                    }
                });
            }
        },
        // 渲染表单组件
        _renderComponents: function(components) {
            var that = this;
            if (components.length) {
                _.each(components, function(component, index) {
                    if (component.src) {
                        require([component.src.indexOf('/') >= 0 ? component.src : ('lib/view/component/' + component.src)], function(View) {
                            HBY.view.create({
                                key: component.key,
                                el: component.target ? ('#' + component.target) : undefined,
                                context: that,
                                view: View,
                                params: component.params || undefined,
                                options: component.options || {}
                            });
                        });
                    }
                });
            }
        },
        // 渲染表单页
        renderAll: function() {
            this.$el.empty();
            var formView = HBY.view.create({
                key: this.id + '_form',
                el: this.$el,
                context: this,
                view: FormView,
                options: this.options.form
            });
            var container = this.$('#' + this.id + '_form'),
                that = this,
                rows = this.options.layout.rows,
                components = this.options.components,
                type = this.options.layout.type;
            if (type) {
                container.addClass('form-' + type);
            }
            container.empty();
            this._renderRows(rows);
            this._renderComponents(components);
            if (this.$(this.options.submitEl).length) {
                this.$(this.options.submitEl).off().on('click', function() {
                    that._submitForm(that.$('form'));
                });
            }
            return this;
        },
        //序列化表单
        _serializeJson: function(formEl) {
            var data = {},
                formEl = formEl instanceof jQuery ? formEl : $(formEl);
            var array = formEl.serializeArray();
            $(array).each(function() {
                data[this.name] = this.value;
            });
            return data;
        },
        //提交表单
        _submitForm: function(formEl) {
            if (formEl.currentTarget) formEl = $(formEl.currentTarget);
            var that = this,
                modelData = this.checkData(this._serializeJson(formEl));
            if (_.isEmpty(modelData)) return false;
            this.savedData(modelData);
        },
        //检验数据
        checkData: function(data) {
            return data;
        },
        // 发送数据
        savedData: function(data) {
            var that = this,
                onSuccess = this.options.onSuccess,
                onError = this.options.onError;
            this.model.create(data, {
                success: function(model, response) {
                    that.$('form')[0].reset();
                    if (_.isFunction(onSuccess)) onSuccess(model, response);
                },
                error: function(model, errorMsg) {
                    if (_.isFunction(onError)) onError(model, errorMsg);
                }
            });
        }
    });
    return View;
});
