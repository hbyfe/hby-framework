/*
 * 路由基类
 * @author: YuRonghui
 * @create: 2015/1/29
 * @update: 2016/1/30
 */
define([
    'lib/HBY'
], function(HBY) {
    // var AppRouter = HBY.Router.extend({

    // });
    return HBY.Router;
});