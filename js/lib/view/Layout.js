/*
 * 布局视图基类
 * @author: yrh
 * @create: 2015/1/29
 * @update: 2016/6/20
*  var config = {
*   guide: {
*
*   },
    layout: {

    },
    widgets: {
        key: {
            src: '',    //插件路径
            name: '',   //插件名
            params: {   //数据源参数
                reset: false,
                url: '',
                data: {}
            },
            target: '', //渲染目标节点ID
            binds: {    //事件交互绑定
                'widgetKey:triggerEvent': 'selfKey:onEvent',
                'widgetKey:triggerEvent': 'selfKey2:onEvent',
            },
            options: {} //其他参数
        }
    }
};
 */
define([
    'lib/view/View',
    'lib/view/element/Row',
    'lib/view/element/Col',
    'lib/view/component/Guide',
], function(BaseView, Row, Col, Guide) {
    var View = BaseView.extend({
        className: 'container-fluid',
        initialize: function(option) {
            var that = this;
            this.bindEvent = {}; //事件存放对象
            this.parent(option);
            // 新手引导
            if (!window.localStorage.getItem(option.key) && this.options.guide) {
                HBY.view.create({
                    key: this.id + "_guide",
                    el: 'body',
                    view: Guide,
                    options: that.options.guide
                });
            }
            // 清除时间选择器
            $('.datetimepicker input').each(function(index, el){
                $(el).datetimepicker('destroy');
            });
            this._renderLayout();
            this._renderWidget();
            this._bindEvent();
        },
        // 渲染布局
        _renderLayout: function() {
            var layout = this.options.layout || {},
                that = this;
            if (layout.rows) {
                _.each(layout.rows, function(row, index) {
                    var theRow = layout.rows[index] || {},
                        rowKey = 'row_' + index,
                        rowOption = {
                            className: 'row',
                        };
                    if (theRow) $.extend(true, rowOption, theRow);
                    var RowView = HBY.view.create({
                        key: rowKey,
                        el: that.$el,
                        view: Row,
                        context: that,
                        options: rowOption
                    });
                    if (row.cols) {
                        var colCount = row.cols.length;
                        if (colCount > 12) colCount = 12;
                        _.each(row.cols, function(col, i) {
                            var theCol = row.cols[i] || {},
                                colKey = rowKey + '-col_' + i,
                                colOption = {
                                    className: col.className ? col.className : ('col-md-' + 12 / colCount),
                                };
                            if (theCol) $.extend(true, colOption, theCol);
                            var ColView = HBY.view.create({
                                key: colKey,
                                el: that.$el.find('#' + rowKey),
                                view: Col,
                                context: that,
                                options: colOption
                            });
                        });
                    }
                });
            }
        },
        // 渲染插件
        _renderWidget: function() {
            var widgets = this.options.widgets || [],
                that = this;
            if (widgets.length) {
                _.each(widgets, function(widget, index) {
                    if (widget.name) {
                        require([widget.src], function(obj) {
                            HBY.view.create({
                                key: widget.key,
                                el: widget.target ? ('#' + widget.target) : undefined,
                                context: that,
                                view: HBY.widgets[widget.name],
                                params: widget.params || undefined,
                                options: widget.options || {},
                            });
                        });
                        if (widget.binds) {
                            _.each(widget.binds, function(val, key) {
                                if (that.bindEvent[key]) {
                                    that.bindEvent[key].push(val);
                                } else {
                                    that.bindEvent[key] = [val];
                                }
                            });
                        }
                    } else {
                        debug.warn(widget.src, '插件名为空的');
                    }
                });
            }
        },
        // 绑定事件
        _bindEvent: function() {
            var that = this;
            _.each(this.bindEvent, function(val, key) {
                if (!that[key]) {
                    that[key] = function(data) {
                        _.each(that.bindEvent[key], function(val, index) {
                            var newVal = that.id + ':' + val;
                            HBY.Events.trigger(newVal, data);
                        });
                    };
                }
                HBY.Events.on(that.id + ':' + key, that[key], that);
            });
        },
        // 触发事件
        trigger: function(event) {
            var thisId = this.id,
                data = arguments.callee.caller.arguments,
                method = arguments.callee.caller.__name;
            _.each(this.bindEvent[method], function(val, index) {
                if (event) {
                    if (event == val) HBY.Events.trigger(thisId + ':' + event, data[0]);
                } else {
                    HBY.Events.trigger(thisId + ':' + val, data[0]);
                }
            });
        }
    });
    return View;
});
