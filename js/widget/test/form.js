define([
    'lib/view/component/Panel',
    'lib/view/component/Form',
    'src/test/model/Chat'
], function(PanelView, FormView, ChatModel) {
    HBY.widgets.form1 = PanelView.extend({
        events: {
            // 'click th': 'onClickBtn',
            // 'click .li_item_css': 'onResultItem'
        },
        initialize: function(option) {
            var that = this;
            var defaults = {
                options: {
                    style: {
                        borderRadius: 0
                    },
                    header: {
                        html: '<h4 class="panel-title text-primary">表单标题' + option.key + '<span></span></h4>',
                        className: 'panel-heading border-light'
                    },
                    body: {
                        html: '<p></p>'
                    },
                    footer: {
                        html: '这是footer',
                        hide: false
                    }
                }
            };
            if (option) $.extend(true, defaults, option || {});
            this.parent(defaults);
            this.parentId = option.context.id;

            var theKey = this.id + '_form';
            HBY.view.create({
                key: theKey,
                el: this.$('.panel-body p'),
                view: FormView,
                context: this,
                model: ChatModel,
                options: {
                    form: {
                        className: 'underline'
                    },
                    layout: {
                        type: 'horizontal',
                        rows: [{
                            isMust: true,
                            label: {
                                text: '名称'
                            },
                            input: {},
                            help: {
                                text: '<i class="ti-info-alt text-primary"></i> A block of help text that breaks onto a new line and may extend beyond one line.'
                            }
                        }, {
                            label: {
                                text: '公司'
                            },
                            input: {},
                        }, {
                            label: {
                                text: '地址'
                            },
                            input: {},
                        }, {
                            label: {
                                text: '时间'
                            },
                            input: {},
                        }, {
                            label: {
                                text: '时间段'
                            },
                            input: {},
                        }, {
                            input: {}
                        }]
                    },
                    components: [{
                        key: 'component_1',
                        src: 'Chosen',
                        target: theKey + '_0',
                        options: {
                            placeholder: '请选择选项',
                            width: '300px',
                            name: 'name',
                            data: [{
                                text: '选项组一',
                                children: [{
                                    text: '一选项',
                                    selected: true
                                }, {
                                    text: '2选项',
                                    disabled: true
                                }, {
                                    text: '3选项'
                                }]
                            }, {
                                text: '选项组二',
                                children: [{
                                    text: '一选项'
                                }, {
                                    text: '2选项'
                                }, {
                                    text: '3选项'
                                }]
                            }, {
                                text: '三选项3'
                            }]
                        }
                    }, {
                        key: 'component_2',
                        src: 'Chosen',
                        target: theKey + '_1',
                        options: {
                            placeholder: '请选择选项',
                            width: '400px',
                            multiple: true,
                            name: 'company',
                            data: [{
                                text: '选项组一',
                                children: [{
                                    text: '一选项',
                                    selected: true
                                }, {
                                    text: '2选项',
                                    disabled: true
                                }, {
                                    text: '3选项'
                                }]
                            }, {
                                text: '选项组二',
                                children: [{
                                    text: '一选项'
                                }, {
                                    text: '2选项'
                                }, {
                                    text: '3选项'
                                }]
                            }, {
                                text: '三选项3'
                            }]
                        }
                    }, {
                        key: 'component_3',
                        src: 'lib/view/element/Input',
                        target: theKey + '_2',
                        options: {
                            placeholder: '请输入内容',
                            type: 'text',
                            name: 'address',
                            required: true,
                            width: 300
                        }
                    }, {
                        key: 'component_4',
                        src: 'Datepicker',
                        target: theKey + '_3',
                        options: {
                            name: 'time',
                            style: {
                                width: '300px'
                            }
                        }
                    }, {
                        key: 'component_5',
                        src: 'Datepicker',
                        target: theKey + '_4',
                        options: {
                            style: {
                                width: '400px'
                            },
                            startInput: {
                                name: 'startDate'
                            },
                            endInput: {
                                name: 'endDate'
                            },
                            isRange: true
                        }
                    }, {
                        key: 'component_6',
                        src: 'lib/view/element/Button',
                        target: theKey + '_5',
                        options: {
                            html: '确定提交',
                            type: 'submit',
                            className: 'btn btn-o btn-primary'
                        }
                    }],
                    onSuccess: function() {
                        console.warn('提交成功');
                    },
                    onError: function() {
                        console.warn('提交失败');
                    }
                }
            });
        }
    });
    return HBY.widgets.form1;
});
