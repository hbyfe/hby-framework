/*
 * 视图基类
 * @author: YuRonghui
 * @create: 2015/1/29
 * @update: 2016/6/20
 */
define([
    'lib/HBY'
], function(HBY) {
    var BaseView = HBY.View.extend({
        io: {}, //输入输出
        asset: {}, //静态资源
        initialize: function(option) {
            var that = this;
            if (option.key) this.el.id = this.id = option.key;
            this.options = option.options || {};
            if (!this.template && this.options.template) {
                if (typeof this.options.template == 'string') {
                    this.template = _.template(this.options.template);
                } else {
                    if (this.options.template instanceof jQuery) {
                        this.template = _.template(this.options.template[0]);
                    }
                }
            }
            if (this.options.className) this.$el.addClass(this.options.className);
            if (this.options.attr) this.$el.attr(this.options.attr);
            if (this.options.title) this.$el.attr('title', this.options.title);
            if (this.options.name) this.$el.attr('name', this.options.name);
            if (this.options.style) this.$el.css(this.options.style);
            if (this.options.html || (this.options.text && this.options.html)) {
                if (_.isObject(this.options.html)) {
                    if (_.isArray(this.options.html)) {
                        this.$el.empty();
                        _.each(this.options.html, function(val, index) {
                            var view = HBY.view.create(val);
                            that.$el.append(view.render().el);
                        });
                    } else {
                        if (_.isFunction(this.options.html)) {
                            this.$el.html(this.options.html());
                        } else {
                            var view = HBY.view.create(this.options.html);
                            this.$el.html(view.render().el);
                        }
                    }
                } else {
                    this.$el.html(this.options.html);
                }
            }
            if (this.options.text && !this.options.html) this.$el.text(this.options.text);
            var currentPage = HBY.getCurrentPage(),
                pageView = HBY.views[currentPage];
            // 样式资源
            if (this.asset.css) {
                _.each(this.asset.css, function(cssSrc, key) {
                    HBY.loadCss(cssSrc.indexOf('http') >= 0 ? cssSrc : (CONFIG.ROOT_URI + cssSrc), pageView);
                });
            }
            // 预加载图片
            if (this.asset.images && pageView) {
                _.each(this.asset.images, function(imgSrc, key) {
                    if (imgSrc) {
                        var img = new Image();
                        img.src = imgSrc.indexOf('http') >= 0 ? imgSrc : (CONFIG.ROOT_URI + imgSrc);
                        img.onload = function() {
                            pageView.$el.append(this);
                        };
                        $(img).hide();
                    }
                });
            }
        },
        // 获取异步内容
        _getData: function(option, id) {
            var that = this,
                defaults = {
                    type: 'GET',
                    dataType: 'html',
                    success: function(result) {
                        that.$(id).html(result);
                    },
                    error: function() {
                        debug.error('获取数据失败');
                    }
                };
            if (option) _.extend(defaults, option || {});
            var url = defaults.url;
            if (url) {
                if (url.indexOf('http') >= 0 || url.indexOf('./') == 0 || url.indexOf('/') == 0) $.ajax(defaults);
            } else {
                return;
            }
        }
    });
    return BaseView;
});