/*
 * 表单元素类
 * @author: yrh
 * @create: 2016/7/19
 * @update: 2016/7/19
 */
define([
    'lib/view/View',
], function(BaseView) {
    var View = BaseView.extend({
        tagName: 'form',
        initialize: function(option) {
            this.parent(option);
            this.$el.attr('onsubmit', 'javascript:return false;');
        }
    });
    return View;
});
