/*
 * 日期选择器通用组件类
 * @author: yrh
 * @create: 2016/7/14
 * @update: 2016/7/14
* options: {
}
 */
define([
    'lib/view/View',
    'lib/view/element/Input',
    'datetimepicker'
], function(BaseView, InputView) {
    var Template = ['<div class="input-group input-daterange datepicker">',
        '<input type="text" class="form-control startDate">',
        '<span class="input-group-addon"><i class="fa fa-long-arrow-right"></i></span>',
        '<input type="text" class="form-control endDate">',
        '</div>'
    ].join('');
    var View = BaseView.extend({
        className: 'datetimepicker',
        template: _.template(Template),
        asset: {
            css: {
                'datetimepicker': '/js/lib/vendor/ui/datetimepicker/jquery.datetimepicker.css'
            }
        },
        initialize: function(option) {
            var that = this,
                defaults = {
                    options: {
                        input: {
                            type: 'text',
                            width: '100%',
                            className: 'form-control',
                            placeholder: '',
                            disabled: false,
                            readonly: false
                        },
                        isRange: false, //是否范围类型
                        startInput: { //开始时间输入框
                            selector: '',
                            name: 'startDate',
                            placeholder: '开始时间',
                            format: 'Y/m/d',
                            closeOnDateSelect: true,
                            timepicker: false
                        },
                        endInput: { //结束时间输入框
                            selector: '',
                            name: 'endDate',
                            placeholder: '结束时间',
                            format: 'Y/m/d',
                            closeOnDateSelect: true,
                            timepicker: false
                        },
                        // parentID: $('#' + HBY.getCurrentPage()),
                        lazyInit: false,
                        value: null,
                        datepicker: true,
                        timepicker: true,
                        format: 'Y/m/d H:i',
                        formatDate: 'Y/m/d',
                        formatTime: 'H:i',
                        lang: 'zh',
                        step: 60,
                        closeOnDateSelect: 0,
                        closeOnWithoutClick: true,
                        validateOnBlur: true,
                        weeks: false,
                        theme: 'default',
                        minDate: false,
                        maxDate: false,
                        defaultDate: false,
                        defaultTime: false,
                        minTime: false,
                        maxTime: false,
                        allowTimes: [],
                        mask: false,
                        opened: false,
                        yearOffset: 0,
                        inline: false,
                        todayButton: true,
                        defaultSelect: true,
                        allowBlank: false,
                        timepickerScrollbar: true,
                        onSelectDate: function() {},
                        onSelectTime: function(current_time, $input) {},
                        onChangeMonth: function(current_time, $input) {},
                        onChangeYear: function(current_time, $input) {},
                        onChangeDateTime: function(current_time, $input) {},
                        onShow: function(current_time, $input) {},
                        onClose: function(current_time, $input) {},
                        onGenerate: function(current_time, $input) {},
                        withoutCopyright: true,
                        inverseButton: false,
                        scrollMonth: true,
                        scrollTime: true,
                        scrollInput: true,
                        hours12: false,
                        yearStart: '1950',
                        yearEnd: '2050',
                        roundTime: 'round',
                        dayOfWeekStart: 0,
                        weekends: [],
                        disabledDates: [],
                        allowDates: [],
                        allowDateRe: [],
                        disabledWeekDays: [],
                        ownerDocument: document,
                        contentWindow: window
                    }
                };
            if (option) $.extend(true, defaults, option);
            this.context = option.context;
            this.parent(defaults);
            this.renderAll();
        },
        renderAll: function() {
            this.$el.empty();
            var that = this,
                theKey = this.id + '_input';
            $.datetimepicker.setLocale('zh');
            var option = {
                key: theKey,
                el: this.$el,
                view: InputView,
                context: this,
                options: this.options.input
            };
            if (!this.options.isRange) {
                HBY.view.create(option);
                this.$('#' + theKey).datetimepicker(this.options);
            } else {
                var startOption = $.extend(true, {}, this.options),
                    endOption = $.extend(true, {}, this.options),
                    startEl = '.startDate',
                    endEl = '.endDate';
                if (!this.options.startInput.selector && !this.options.startInput.selector) {
                    this.$el.html(this.template());
                    _.extend(startOption, this.options.startInput);
                    startOption.onShow = function(ct) {
                        this.setOptions({
                            maxDate: that.$(endEl).val() ? that.$(endEl).val() : false
                        });
                    };
                    _.extend(endOption, this.options.endInput);
                    endOption.onShow = function(ct) {
                        this.setOptions({
                            minDate: that.$(startEl).val() ? that.$(startEl).val() : false
                        });
                    };
                } else {
                    if (this.options.startInput.selector) {
                        _.extend(startOption, this.options.startInput);
                        startEl = this.options.startInput.selector;
                        startOption.onShow = function(ct) {
                            this.setOptions({
                                maxDate: that.$(endEl).val() ? that.$(endEl).val() : false
                            });
                        };
                    }
                    if (this.options.startInput.selector) {
                        _.extend(endOption, this.options.endInput);
                        endEl = this.options.startInput.selector;
                        endOption.onShow = function(ct) {
                            this.setOptions({
                                minDate: that.$(startEl).val() ? that.$(startEl).val() : false
                            });
                        };
                    }
                }
                if (this.options.startInput.name) this.$(startEl).attr('name', this.options.startInput.name);
                if (this.options.endInput.name) this.$(endEl).attr('name', this.options.endInput.name);
                if (this.options.startInput.placeholder) this.$(startEl).attr('placeholder', this.options.startInput.placeholder);
                if (this.options.endInput.placeholder) this.$(endEl).attr('placeholder', this.options.endInput.placeholder);
                this.$(startEl).datetimepicker(startOption);
                this.$(endEl).datetimepicker(endOption);
            }
            $('.xdsoft_datetimepicker').hide();
            $(CONFIG.PAGE_EL).scroll(function() {
                that.$('input').datetimepicker('hide');
            });
        }
    });
    return View;
});
