/*
 * 日期选择器通用组件类
 * @author: yrh
 * @create: 2016/7/14
 * @update: 2016/7/14
* options: {
}
 */
define([
    'lib/view/View',
    'lib/view/component/Select',
    'chosen'
], function(BaseView, SelectView) {
    var View = BaseView.extend({
        className: 'chosen-select',
        asset: {
            css: {
                'chosen': '/js/lib/vendor/ui/chosen/chosen.css'
            }
        },
        initialize: function(option) {
            var that = this,
                defaults = {
                    options: {
                        className: '',
                        multiple: false, //是否多选
                        placeholder: '',
                        width: '100px',
                        search_api: null, //搜索接口
                        allow_single_deselect: false, //是否显示单选删除
                        disable_search: false, //不在搜索结果(单选)
                        disable_search_threshold: 0, //隐藏在单选择搜索输入如果有N个或更少的选项。
                        enable_split_word_search: true, //默认情况下，搜索将匹配上的一个选项标签中的任何字
                        inherit_select_classes: false, //是否继承选择组件的样式，并将其添加到选上的容器div
                        max_selected_options: 100, //限制最大选择项数。 当达到限制时， chosen:maxselected触发事件。
                        no_results_text: "No results match",
                        search_contains: false, //默认情况下，所选择的搜索开始匹配一个单词的开头
                        single_backstroke_delete: true, //按下多重选择删除/退格将删除选定的选择
                        display_disabled_options: true, //是否显示禁用的选项
                        display_selected_options: true, //是否显示选中的选项
                        include_group_label_in_selected: false, //是否显示组
                        max_shown_results: 100, //显示最大结果数
                        data: []
                    }
                };
            if (option) $.extend(true, defaults, option);
            this.context = option.context;
            this.datas = {};
            this.parent(defaults);
            if (this.options.multiple) {
                this.options.placeholder_text_multiple = this.options.placeholder;
            } else {
                this.options.placeholder_text_single = this.options.placeholder;
            }
            if (this.options.width) this.$el.width(this.options.width);
            if (option.collection) {
                this.collection = option.collection;
                // 获取数据
                this.stopListening(this.collection);
                this.listenTo(this.collection, "remove", this._makeData);
                this.listenTo(this.collection, "reset", this._makeData);
                HBY.Events.off(this.collection.key);
                HBY.Events.on(this.collection.key, this._makeData, this);
            } else {
                this.renderAll();
            }
        },
        // 组装数据集数据
        _makeData: function(collection) {
            var newData = [];
            this.collection.each(function(model, index) {
                newData.push(model.attributes);
            });
            this.options.data = newData;
            this.renderAll();
        },
        _getDirection: function(el) {
            var _X = el.offset().left,
                _Y = el.offset().top,
                windowHeight = $(window).height(),
                elHeight = el.height();
            if (el.children('.chosen-drop').height() > (windowHeight - _Y - elHeight)) {
                return 'up';
            } else {
                return 'bottom';
            }
        },
        // 渲染视图
        renderAll: function() {
            var that = this,
                theKey = this.id + '_select';
            if (this.options.data.length) {
                HBY.view.create({
                    key: theKey,
                    el: this.$el,
                    context: this,
                    view: SelectView,
                    inset: 'html',
                    options: this.options,
                    onInitAfter: function(key, context) {
                        that.$('#' + theKey).chosen(that.options);
                        that.$('.chosen-container-single .chosen-single div').html('<i class="fa fa-sort-desc"></i>');
                        that.$('.chosen-search').append('<i class="fa fa-search"></i>');
                        that.$('#' + theKey).on('chosen:showing_dropdown', function(evt, params) {
                            var position = that._getDirection($(this).next());
                            $(this).next().addClass(position);
                        }).on('chosen:hiding_dropdown', function(evt, params){
                            $(this).next().removeClass('up bottom');
                        });
                    }
                });
            }
        }
    });
    return View;
});