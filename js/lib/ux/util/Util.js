/**
 * 业务扩展函数库
 */
define([
    "lib/HBY",
    "lib/ux/util/Helper",
    "lib/ux/util/Qiniu",
    "lib/ux/util/IM",
    "lib/ux/util/Permit",
], function(HBY, Helper, Qiniu, IM, Permit) {
    var Util = HBY.ns('HBY.ux.util', HBY.Base.extend({
        Helper: Helper, //辅助工具
        Qiniu: Qiniu,   //七牛
        IM: IM, //聊天
        Permit: Permit, //权限
    }));
    return Util;
});
