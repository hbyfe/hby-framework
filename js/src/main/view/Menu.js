/**
 * 系统菜单视图类
 * @class menu
 */
define([
    'lib/view/View',
    'text!src/main/template/menu.html'
], function(BaseView, TplMenu) {
    var View = BaseView.extend({
        el: "#sidebar",
        template: _.template(TplMenu),
        events: {
            'click .main-navigation-menu > li': "openNav",
            // 'click #chat > .mask-layer': 'hideChat'
        },
        initialize: function() {
            this.$el.html(this.template());
        },
        // 打开菜单
        openNav: function(event){
            var target = $(event.currentTarget);
            target.addClass('active open').siblings('li').removeClass('active open');
        }
    });
    return View;
});
