/**
 * # 描述
 * 系统主框架视图类
 * @class main
 */
define([
    'lib/HBY',
    'lib/view/View',
    'src/main/view/Menu',
    'perfect-scrollbar'
], function(HBY, BaseView, MenuView) {
    var View = BaseView.extend({
        el: "body",
        events: {
            'click .sidebar-toggler': 'closeSidebar', //隐藏显示左边菜单
            // 'click .menu-search': 'clickSearchBtn',
            // 'click .head-input-search': 'onkeydownSearch'
        },
        initialize: function() {
            new MenuView();
            // new LayoutView();
            if (CONFIG.IS_DEBUG) {
                // HBY.loadCss(static_url + '/css/common.css?v=1');
                // HBY.loadCss(static_url + '/css/debug.css?v=1');
                this.$('.tabpanel-container').addClass('debug-bg');
            }
            // 加载IM
            // require([CONFIG.IM_APP_SRC_V2]);

            var $html = $('html'),
                $win = $(window),
                wrap = $('.app-aside'),
                MEDIAQUERY = {},
                app = $('#app');

            MEDIAQUERY = {
                desktopXL: 1200,
                desktop: 992,
                tablet: 768,
                mobile: 480
            };
            //sidebar
            var sidebarHandler = function() {
                var eventObject = isTouch() ? 'click' : 'mouseenter',
                    elem = $('#sidebar'),
                    ul = "",
                    menuTitle, _this;

                elem.on('click', 'a', function(e) {
                    _this = $(this);
                    if (isSidebarClosed() && !isSmallDevice() && !_this.closest("ul").hasClass("sub-menu"))
                        return;
                    _this.closest("ul").find(".open").not(".active").children("ul").not(_this.next()).slideUp(200).parent('.open').removeClass("open");
                    if (_this.next().is('ul') && _this.parent().toggleClass('open')) {
                        _this.next().slideToggle(200, function() {
                            $win.trigger("resize");
                        });
                        e.stopPropagation();
                        e.preventDefault();
                    } else {
                        _this.parent().addClass("active");
                    }
                });
                elem.on(eventObject, 'a', function(e) {
                    if (!isSidebarClosed() || isSmallDevice())
                        return;
                    _this = $(this);

                    if (!_this.parent().hasClass('hover') && !_this.closest("ul").hasClass("sub-menu")) {
                        wrapLeave();
                        _this.parent().addClass('hover');
                        menuTitle = _this.find(".item-inner").clone();
                        if (_this.parent().hasClass('active')) {
                            menuTitle.addClass("active");
                        }
                        var offset = $("#sidebar").position().top;
                        var itemTop = isSidebarFixed() ? _this.parent().position().top + offset : (_this.parent().position().top);
                        menuTitle.css({
                            position: isSidebarFixed() ? 'fixed' : 'absolute',
                            height: _this.outerHeight(),
                            top: itemTop
                        }).appendTo(wrap);
                        if (_this.next().is('ul')) {
                            ul = _this.next().clone(true);
                            ul.appendTo(wrap).css({
                                top: menuTitle.position().top + _this.outerHeight(),
                                position: isSidebarFixed() ? 'fixed' : 'absolute',
                            });
                            if (_this.parent().position().top + _this.outerHeight() + offset + ul.height() > $win.height() && isSidebarFixed()) {
                                ul.css('bottom', 0);
                            } else {
                                ul.css('bottom', 'auto');
                            }
                            wrap.children().first().scroll(function() {
                                if (isSidebarFixed())
                                    wrapLeave();
                            });
                            setTimeout(function() {
                                if (!wrap.is(':empty')) {
                                    $(document).on('click tap', wrapLeave);
                                }
                            }, 300);
                        } else {
                            ul = "";
                            return;
                        }
                    }
                });
                wrap.on('mouseleave', function(e) {
                    $(document).off('click tap', wrapLeave);
                    $('.hover', wrap).removeClass('hover');
                    $('> .item-inner', wrap).remove();
                    $('> ul', wrap).remove();
                });
            };
            // navbar collapse
            var navbarHandler = function() {
                var navbar = $('.navbar-collapse > .nav');
                var pageHeight = $win.innerHeight() - $('header').outerHeight();
                var collapseButton = $('#menu-toggler');
                if (isSmallDevice()) {
                    navbar.css({
                        height: pageHeight
                    });
                } else {
                    navbar.css({
                        height: 'auto'
                    });
                };
                $(document).on("mousedown touchstart", toggleNavbar);
                function toggleNavbar(e) {
                    if (navbar.has(e.target).length === 0 //checks if descendants of $box was clicked
                        && !navbar.is(e.target) //checks if the $box itself was clicked
                        && navbar.parent().hasClass("collapse in")) {
                        collapseButton.trigger("click");
                    }
                };
            };
            // perfect scrollbar
            var perfectScrollbarHandler = function() {
                var pScroll = $(".perfect-scrollbar");
                if (!isMobile() && pScroll.length) {
                    pScroll.perfectScrollbar({
                        suppressScrollX: true
                    });
                    pScroll.on("mousemove", function() {
                        $(this).perfectScrollbar('update');
                    });
                }
            };
            //search form
            var searchHandler = function() {
                var elem = $('.search-form');
                var searchForm = elem.children('form');
                var formWrap = elem.parent();

                $(".s-open").on('click', function(e) {
                    searchForm.prependTo(wrap);
                    e.preventDefault();
                    $(document).on("mousedown touchstart", closeForm);
                });
                $(".s-remove").on('click', function(e) {
                    searchForm.appendTo(elem);
                    e.preventDefault();
                });
                var closeForm = function(e) {
                    if (!searchForm.is(e.target) && searchForm.has(e.target).length === 0) {
                        $(".s-remove").trigger('click');
                        $(document).off("mousedown touchstart", closeForm);
                    }
                };
            };
            // Window Resize Function
            var resizeHandler = function(func, threshold, execAsap) {
                $(window).resize(function() {
                    navbarHandler();
                });
            };

            function wrapLeave() {
                wrap.trigger('mouseleave');
            }

            function isTouch() {
                return $html.hasClass('touch');
            }

            function isSmallDevice() {
                return $win.width() < MEDIAQUERY.desktop;
            }

            function isSidebarClosed() {
                return $('.app-sidebar-closed').length;
            }

            function isSidebarFixed() {
                return $('.app-sidebar-fixed').length;
            }

            function isMobile() {
                if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                    return true;
                } else {
                    return false;
                };
            }

            sidebarHandler();
            navbarHandler();
            searchHandler();
            perfectScrollbarHandler();
            resizeHandler();
        },
        //隐藏显示左边菜单
        closeSidebar: function(event) {
            $('#app').toggleClass('app-sidebar-closed');
            var menuDiv = $('#sidebar');
            if (!$('#app').hasClass('app-sidebar-closed')) {
                menuDiv.off('mouseenter.menu mouseleave.menu');
            }
        },
        // 系统搜索
        startSearch: function() {
            var searchBox = this.$(".head-input-search"),
                itemObj = searchBox.parent(".thm-item"),
                value = $.trim(searchBox.val());
            if (value) {
                if (value.length > 50) {
                    HBY.util.System.showMsg('warning', "关键字不能超过50个字符！");
                } else {
                    window.location.href = "/search?key=" + encodeURIComponent(value);
                }
            } else {
                HBY.util.System.showMsg('warning', "请输入搜索条件！");
            }
        }
    });
    return View;
});
