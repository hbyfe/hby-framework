define([
    'lib/router/Router',
    'src/test/view/Test',
    'src/test/view/Form'
], function(BaseController, TestView, FormView) {
    var Controller = BaseController.extend({
        routes: {
            "test/index": "home",
            "test/page_2": "page_2"
        },
        initialize: function() {
            this.home();
        },
        home: function(id) {
            HBY.view.create({
                key: "page_demo",
                view: TestView,
                inset: 'html',
            });
        },
        page_2: function(id) {
            HBY.view.create({
                key: "page_2",
                view: FormView,
                inset: 'html',
            });
        }
    });
    return Controller;
});
