define([
    'lib/view/Layout'
], function(LayoutView) {
    var View = LayoutView.extend({
        initialize: function(option) {
            var defaults = {
                options: {
                    style: {
                        marginTop: '15px',
                    },
                    layout: {
                        rows: [{
                            cols: [{}]
                        }]
                    },
                    widgets: [{
                        key: 'panel_1',
                        src: 'widget/test/form',
                        name: 'form1',
                        target: 'row_0-col_0',
                        options: {
                            className: 'panel panel-white'
                        }
                    }]
                }
            };
            if (option) $.extend(true, defaults, option || {});
            this.parent(defaults);
        }
    });
    return View;
});
