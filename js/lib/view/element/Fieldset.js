/*
 * Fieldset元素类
 * @author: yrh
 * @create: 2016/7/19
 * @update: 2016/7/19  `
 */
define([
    'lib/view/View',
], function(BaseView) {
    var View = BaseView.extend({
    	tagName: 'fieldset',
        initialize: function(option) {
            this.parent(option);
        }
    });
    return View;
});
