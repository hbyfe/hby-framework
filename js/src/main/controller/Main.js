/*
 * 系统桌面控制器
 * YuRonghui
 * 2016/1/8 update 2016/1/8
 */
define([
    'lib/router/Router',
    'src/main/view/Main',
    // "src/main/dataproxy/Main",
], function(BaseController, MainView) {
    return BaseController.extend({
        routes: {
            ":app(/*path)": "loadApp",
        },
        initialize: function() {
            // HBY.currentModule = 'home';
            // 创建布局框架
            new MainView();
            if(CONFIG.DEFAULT_APP) window.location.hash = '#' + CONFIG.DEFAULT_APP;
        },
        //加载应用模块启动文件 YuRonghui 2016/1/8 update 2016/1/8
        loadApp: function(app) {
            // this.navigate("");
            var that = this,
                theAppUrl = (CONFIG.APPS_PATH[app] ? CONFIG.APPS_PATH[app] : (CONFIG.ROOT_URI + '/js/src/' + app));
            HBY.loadApp(theAppUrl + '/app.js',
                function() {
                    // debug.warn('加载前操作');
                },
                function() {
                    
                });
        }
    });
});
